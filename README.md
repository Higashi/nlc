Calculator app. To run application type:

    ./gradlew run --args "<args>"

where `<args>` mathematical computation you want to count. Remember about quotations marks, as they are mandatory with gradle. 

Application recognizes numbers one to ten and mathematical operations: adding, subtracting, dividing, and multiplying.

ex:

    ./gradlew run --args "one plus one"  

To suppress most gradle messages (except errors), add `--quiet` param, resulting:

    ./gradlew run --quiet --args "one plus one"
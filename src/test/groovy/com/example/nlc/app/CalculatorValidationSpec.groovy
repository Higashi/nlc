package com.example.nlc.app

import groovy.util.logging.Log4j2
import spock.lang.Specification
import spock.lang.Unroll

@Log4j2
class CalculatorValidationSpec extends Specification {

    @Unroll
    def "Calculator validate method should return #expected for given args: #args"() {
        given:
        Calculator calc = new Calculator(args)
        when:
        List<String> resultList = calc.validate(args)
        then:
        resultList == expected
        where:
            expected                                     || args
            ['one', 'add', 'one']                        || ['one', 'add', 'one']
            ['one', 'plus', 'two','divided-by','three']  || ['one','plus', 'two','divided-by','three']
    }

    @Unroll
    def "Calculator validate method should throw exception for invalid args: #expectedValidationErrors"() {
        when:
            new Calculator(args)
        then:
            def ex = thrown(IllegalArgumentException)
            ex.message.startsWith(errorMsg)
        where:
        errorMsg || args
        "Failed to recognize argument: 'plus'."  || ['plus', 'add', 'one']
        "Failed to recognize argument: 'plus'."  || ['one', 'plus', 'plus']
    }
}

package com.example.nlc.app.util

import groovy.util.logging.Log4j2
import spock.lang.Specification

import static com.example.nlc.app.util.MathOperationFactory.ADD
import static com.example.nlc.app.util.MathOperationFactory.DIVIDE
import static com.example.nlc.app.util.MathOperationFactory.MULTIPLY
import static com.example.nlc.app.util.MathOperationFactory.SUBTRACT

@Log4j2
class MathOperationFactorySpec extends Specification {

    def "String #operation should convert to #expected enum"() {
        when:
        def result = MathOperationFactory.parse(operation)
        then:
        expected == result
        where:
        expected || operation
        ADD      || 'add'
        ADD      || 'ADD'
        ADD      || 'plus'
        SUBTRACT || 'minus'
        SUBTRACT || 'subtract'
        SUBTRACT || 'SUBTRACT'
        MULTIPLY || 'multiply'
        MULTIPLY || 'MULTIPLY'
        MULTIPLY || 'times'
        DIVIDE   || 'divide'
        DIVIDE   || 'divided-by'
    }
}

package com.example.nlc.app.calculator.service

import groovy.util.logging.Log4j
import groovy.util.logging.Log4j2
import spock.lang.Specification
import spock.lang.Unroll

@Log4j2
class CalculatorServiceSpec extends Specification {

    @Unroll
    def "adding two numbers together should result in: #result"() {
        given:
        CalculatorService service = new CalculatorService(args)
        when:
        Double computed = service.compute()
        then:
        expected == computed
        where:
        expected || args
        1d+1d    || ['one','plus','one']
        1d-1d    || ['one','minus','one']
        1d*1d    || ['one','times','one']
        1d/1d    || ['one',"divided-by",'one']
    }

    @Unroll
    def "Operating on three numbers together should result in: #result"() {
        given:
        CalculatorService service = new CalculatorService(args)
        when:
        Double computed = service.compute()
        then:
        expected == computed
        where:
        expected || args
        1d+1d+1d || ['one','plus','one', 'plus', 'one']
        1d-1d-1d || ['one','minus', 'one', 'minus', 'one']
        2d*2d*2d || ['two','times','two','times','two']
        8d/2d/2d || ['eight','divided-by','two', 'divided-by', 'two']
    }

    @Unroll
    def "Computing numbers with mixed priority should result in: #result"() {
        given:
        CalculatorService service = new CalculatorService(args)
        when:
        Double computed = service.compute()
        then:
        expected == computed
        where:
        expected || args
        1d+4d*4d || ['one','plus','four', 'times', 'four']
        1d-4d/2d || ['one','minus', 'four', 'divide', 'two']
        2d*2d-2d || ['two','times','two','minus','two']
        8d/2d-2d || ['eight','divided-by','two', 'minus', 'two']
        2.78     || ['seven', 'over', 'nine', 'plus', 'two']
    }

    @Unroll
    def "Computing example arguments should result: #result"() {
        given:
        CalculatorService service = new CalculatorService(args)
        when:
        Double computed = service.compute()
        then:
        expected == computed
        where:
        expected || args
        3.79d || ['nine', 'over', 'eight', 'plus', 'four', 'times', 'two', 'divided-by', 'three']
        7d   || ['one','plus', 'six']
        7d   || ['one','plus', 'two', 'times', 'three']
        -12d || ['nine','minus','three','times','seven']
        50d  || ['four', 'minus', 'eight', 'plus', 'six', 'times', 'nine']
        2.78d || ['seven', 'over', 'nine', 'plus', 'two']
    }

}

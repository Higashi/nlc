package com.example.nlc.app.model

import groovy.util.logging.Log4j2
import spock.lang.Specification

@Log4j2
class DivideScaleSpec extends Specification {

    def "Divided number should have scale of 2"() {
        when:
        def result = DivideOperation.divide(7d, 9d).count()
        then:
        result == 0.78d
    }

}

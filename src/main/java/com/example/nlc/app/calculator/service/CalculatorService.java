package com.example.nlc.app.calculator.service;

import com.example.nlc.app.util.MathOperationFactory;
import com.example.nlc.app.util.Numbers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class CalculatorService {
    private static final Logger log = LogManager.getLogger("CalculatorService");
    private final LinkedList<String> computationQueue;

    public CalculatorService(List<String> computations) {
        this.computationQueue = new LinkedList<>(computations);
    }

    public double compute() {
        log.info("Computing queue: {}", computationQueue);
        while (!computationQueue.isEmpty()) {
            //order is important:
            Double left = Numbers.getNumber(computationQueue.pop());
            MathOperationFactory operation = MathOperationFactory.parse(computationQueue.pop());
            Double right = Numbers.getNumber(computationQueue.pop());
            if (computationQueue.isEmpty()) {
                return operation.count(left, right);
            }
            if (isNotPrecedence(operation) && isNextComputationPrecedence()) {
                MathOperationFactory nextOperation = MathOperationFactory.parse(computationQueue.pop());
                Double nextRight = Numbers.getNumber(computationQueue.pop());
                computationQueue.addFirst((nextOperation.count(right, nextRight)).toString());
                computationQueue.addFirst(operation.toString());
                computationQueue.addFirst(left.toString());
            } else {
                computationQueue.addFirst(String.valueOf(operation.count(left, right)));
            }
        }
        log.error("Computation failed for arguments: {}. Queue empty and result not returned", computationQueue);
        throw new IllegalComputationState("Failed count result for given arguments");
    }

    private boolean isNotPrecedence(MathOperationFactory operation) {
        return !MathOperationFactory.priorityOperations.contains(operation);
    }

    private boolean isNextComputationPrecedence() {
        return MathOperationFactory.priorityOperations.contains(MathOperationFactory.parse(computationQueue.peek()));
    }
}

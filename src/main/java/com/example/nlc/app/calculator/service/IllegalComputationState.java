package com.example.nlc.app.calculator.service;

public class IllegalComputationState extends RuntimeException {

    public IllegalComputationState(String msg) {
        super(msg);
    }
}

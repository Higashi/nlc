package com.example.nlc.app;

import com.example.nlc.app.calculator.service.CalculatorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.IntStream;

import static com.example.nlc.app.util.WordProcessor.isNumber;
import static com.example.nlc.app.util.WordProcessor.isOperation;
import static java.lang.String.join;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class Calculator {
    private static final Logger log = LogManager.getLogger("Calculator");
    public static void main(String... args) {
        log.info("Starting calculator application. Have a nice counting!\n\n");
        requireNonNull(args, () -> {
            throw new IllegalArgumentException("This program requires arguments to work correctly");
        });
        if (args.length <= 2) {
            log.error("Provided wrong argument number. Provided: {}. At least three arguments are required in mathematical operations, ex: one plus one.", args.length);
            throw new IllegalArgumentException("Wrong arguments number. Must be at least three!");
        }
        new Calculator(asList(args));
    }

    public Calculator(List<String> args) {
        List<String> validArguments = validate(args);
        log.info("Validated and normalized arguments: {}", join(",", validArguments));
        new CalculatorService(validArguments).compute();
    }

    private List<String> validate(List<String> args) {
        List<String> invalidArguments = IntStream.range(0, args.size())
                .peek(i -> log.debug("Verifying if provided argument '{}' is: {}", args.get(i),
                        (i % 2 == 0 ? "number" : "arithmetic operation" ) ))
                // verify if even arguments are arithmetic numbers, and odd arguments are operators.
                .filter(i -> (i % 2 == 0) ? !isNumber(args.get(i)) : !isOperation(args.get(i)))
                .mapToObj(args::get)
                .collect(toList());
        if (!invalidArguments.isEmpty()) {
            throw new IllegalArgumentException(buildInvalidArgsMessage(invalidArguments));
        }
        return args.stream().map(arg -> arg.trim().toLowerCase()).collect(toList());
    }

    private String buildInvalidArgsMessage(List<String> invalidArguments) {
        boolean multiplyArgs = invalidArguments.size() > 1;
        return "Failed to recognize argument" + (multiplyArgs ? "s: '" : ": '")  + join(",", invalidArguments)
                + "'. Not a number, arithmetic operation or in wrong position.";
    }
}

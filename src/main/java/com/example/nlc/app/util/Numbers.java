package com.example.nlc.app.util;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class Numbers {
    //hide constructor to prevent creating instance of util class
    private Numbers() {}

    private static final Map<String, Integer> numberConverter = new HashMap(){
        {
            put("one", 1);
            put("two", 2);
            put("three", 3);
            put("four", 4);
            put("five", 5);
            put("six", 6);
            put("seven", 7);
            put("eight", 8);
            put("nine", 9);
            put("ten", 10);
        }
    };
//    private static final List<String> numbers = new ArrayList<>(numberConverter.keySet());

    public static boolean isNumber(String num) {
        requireNonNull(num, "Provided argument cannot be null");
        return numberConverter.containsKey(num);
    }

    public static Double getNumber(String word) {
        String stringNumber = numberConverter.containsKey(word) ? numberConverter.get(word).toString() : word;
        return Double.valueOf(stringNumber);
    }
}

package com.example.nlc.app.util;

public class WordProcessor {
    //hide constructor to prevent creating instance of util class
    private WordProcessor() {}

    public static boolean isNumber(String word) {
        return word != null && Numbers.isNumber(word);
    }

    public static boolean isOperation(String word) {
        return word != null && MathOperationFactory.isOperator(word);
    }

}

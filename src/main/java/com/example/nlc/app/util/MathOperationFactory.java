package com.example.nlc.app.util;

import com.example.nlc.app.model.AddOperation;
import com.example.nlc.app.model.Countable;
import com.example.nlc.app.model.DivideOperation;
import com.example.nlc.app.model.MultiplyOperation;
import com.example.nlc.app.model.SubtractOperation;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public enum MathOperationFactory implements Countable {
    ADD("add,plus") {
        @Override
        public Double count(Double left, Double right) {
            return AddOperation.add(left, right).count();
        }
    },
    SUBTRACT("subtract,minus") {
        @Override
        public Double count(Double left, Double right) {
            return SubtractOperation.subtract(left, right).count();
        }
    },
    MULTIPLY("multiply,times") {
        @Override
        public Double count(Double left, Double right) {
            return MultiplyOperation.multiply(left, right).count();
        }
    },
    DIVIDE("divide,divided-by,over") {
        @Override
        public Double count(Double left, Double right) {
            return DivideOperation.divide(left, right).count();
        }
    };

    public static final List<MathOperationFactory> priorityOperations = List.of(MULTIPLY, DIVIDE);

    String operation;

    MathOperationFactory(String operation) {
        this.operation = operation;
    }

    public static boolean isOperator(String word) {
        return Arrays.stream(MathOperationFactory.values())
                .flatMap(o -> Stream.of(o.operation.split(",")))
                .anyMatch(operation -> operation.equalsIgnoreCase(word.trim()));
    }

    public static MathOperationFactory parse(String value) {
        Objects.requireNonNull(value, "Cannot convert null value to enum");
        return Arrays.stream(MathOperationFactory.values())
                .filter(e -> e.operation.contains(value.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

}

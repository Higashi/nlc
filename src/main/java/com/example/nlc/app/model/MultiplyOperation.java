package com.example.nlc.app.model;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;

public class MultiplyOperation extends BaseOperation implements Value {

    private MultiplyOperation(Double left, Double right) {
        super(left, right);
    }
    public static Value multiply(String left, String right) {
        return MultiplyOperation.multiply(Double.valueOf(left), Double.valueOf(right));
    }
    public static Value multiply(double left, double right) {
        return new MultiplyOperation(left, right);
    }

    @Override
    public Double count() {
        return valueOf(this.left).multiply(valueOf(this.right)).setScale(2, HALF_UP).doubleValue();
    }
}

package com.example.nlc.app.model;

public interface Countable {

    Double count(Double left, Double right);
}

package com.example.nlc.app.model;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;

public class SubtractOperation extends BaseOperation implements Value {

    private SubtractOperation(Double left, Double right) {
        super(left, right);
    }
    public static Value subtract(int left, int right) {
        return SubtractOperation.subtract((double)left, (double)right);
    }
    public static Value subtract(String left, String right) {
        return SubtractOperation.subtract(Double.valueOf(left), Double.valueOf(right));
    }
    public static Value subtract(Double left, Double right) {
        return new SubtractOperation(left, right);
    }

    @Override
    public Double count() {
        return valueOf(left)
                .subtract(valueOf(right))
                .setScale(2, HALF_UP)
                .doubleValue();
    }
}

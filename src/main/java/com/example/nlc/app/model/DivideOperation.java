package com.example.nlc.app.model;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_DOWN;

public class DivideOperation extends BaseOperation implements Value {

    private DivideOperation(Double left, Double right) {
        super(left, right);
    }
    public static Value divide(String left, String right) {
        return DivideOperation.divide(Double.valueOf(left), Double.valueOf(right));
    }
    public static Value divide(Double left, Double right) {
        return new DivideOperation(left, right);
    }

    @Override
    public Double count() {
        return valueOf(this.left).divide(valueOf(this.right), 2, HALF_DOWN).doubleValue();
    }
}

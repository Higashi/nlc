package com.example.nlc.app.model;

public abstract class BaseOperation {
    protected Double left;
    protected Double right;

    public BaseOperation(double left, double right) {
        this.left = left;
        this.right = right;
    }
}

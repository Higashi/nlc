package com.example.nlc.app.model;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;

public class AddOperation extends BaseOperation implements Value {

    private AddOperation(Double left, Double right) {
        super(left, right);
    }

    public static Value add(int left, int right) {
        return AddOperation.add((double)left, (double)right);
    }

    public static Value add(String left, String right) {
        return AddOperation.add(Double.valueOf(left), Double.valueOf(right));
    }
    public static Value add(Double left, Double right) {
        return new AddOperation(left, right);
    }

    @Override
    public Double count() {
        return valueOf(Double.sum(this.left, this.right)).setScale(2, HALF_UP).doubleValue();
    }
}
